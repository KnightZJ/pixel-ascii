#include <stdio.h>

#define MAX_WIDTH  1200
#define MAX_HEIGHT 2000

struct Pixel {
  unsigned char r, g, b;
};

struct Pixel raw_image[MAX_HEIGHT][MAX_WIDTH];
unsigned char gray_image[MAX_HEIGHT][MAX_WIDTH];
unsigned char final_image[MAX_HEIGHT][MAX_WIDTH];
int image_width, image_height;

int read_image(char* path) {
  FILE *f = fopen(path, "r");
  if (f == NULL) {
    printf("Failed to open file %s!\n", path);
    return -1;
  }
  if (fgetc(f) == 'P' && fgetc(f) == '3') {
    int width, height, type;
    fscanf(f, "%d%d%d", &width, &height, &type);
    image_width = width;
    image_height = height;
    printf("%d %d\n", width, height);
    for (int i = 0; i < height; ++i)
      for (int j = 0; j < width; ++j) {
        fscanf(f, "%d%d%d", &raw_image[i][j].r,
                            &raw_image[i][j].g,
                            &raw_image[i][j].b);
        //printf("(%d %d %d)\t", raw_image[i][j].r, raw_image[i][j].g, raw_image[i][j].b);
      }
  }
  fclose(f);
  return 0;
}

int to_gray() {
  for (int i = 0; i < image_height; ++i) {
    for (int j = 0; j < image_width; ++j) {
      gray_image[i][j] = (raw_image[i][j].r/3+raw_image[i][j].g/3+raw_image[i][j].b/3);
      // printf("%d \t", gray_image[i][j]);
    }
    printf("\n");

  }
  return 0;
}

int shrink() {
  for (int i = 0; i < image_height/5; ++i)
    for (int j = 0; j < image_width/2; ++j) {
      int cnt = 0;
      for (int dx = 0; dx < 2; ++dx)
        for (int dy = 0; dy < 5; ++dy)
          cnt += gray_image[i*5+dy][j*2+dx];
      final_image[i][j] = cnt/10;
    }
  return 0;
}

char output(unsigned char gray) {
  if (gray < 50)
    return '#';
  else if (gray < 100)
    return '@';
  else if (gray < 150)
    return '(';
  else if (gray < 200)
    return '|';
  else
    return ' ';
}

int main() {
  read_image("../image/baizi2.ppm");
  to_gray();
  shrink();
  for (int i = 0; i < image_height/5; ++i) {
    for (int j = 0; j < image_width/2; ++j) {
      printf("%c", output(final_image[i][j]));

    }
    printf("\n");
  }
  return 0;
}
